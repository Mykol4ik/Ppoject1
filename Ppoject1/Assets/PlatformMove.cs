﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMove : MonoBehaviour
{
    public bool MoveX;
    public bool MoveY;
    public bool MoveZ;
    public float MoveStepX;
    public float MoveStepY;
    public float MoveStepZ;
    float NapryamX = 1, NapryamY = 1, NapryamZ = 1;
    public float OffSetX, OffSetY, OffSetZ = 0;
    public float MaxOffSetX, MaxOffSetY, MaxOffSetZ;
    Transform StartPosition;
    Transform TargetPosition;

    void Start()
    {
        StartPosition = this.gameObject.transform;
    }

    void Update()
    {
        if (OffSetX > MaxOffSetX)
            NapryamX = -1;
        if (OffSetX < 0)
            NapryamX = 1;

        OffSetX = OffSetX + (MoveStepX * NapryamX);
        transform.position = new Vector3(OffSetX, OffSetY, OffSetZ);
    }
}
