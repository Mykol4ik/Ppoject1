﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{

    int BallJumpPower = 5000;
    int BallGoPower = 50;
    bool IsGrounded;
    Rigidbody Ball;

    void Start()
    {
        Ball = this.gameObject.GetComponent<Rigidbody>();
        Ball.mass = 5;
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded)
            Ball.AddForce(Vector3.up * BallJumpPower);
        if (Input.GetKey(KeyCode.W))
            Ball.AddForce(Vector3.forward * BallGoPower);
        if (Input.GetKey(KeyCode.S))
            Ball.AddForce(Vector3.back * BallGoPower);
        if (Input.GetKey(KeyCode.D))
            Ball.AddForce(Vector3.right * BallGoPower);
        if (Input.GetKey(KeyCode.A))
            Ball.AddForce(Vector3.left * BallGoPower);

    }
    private void OnCollisionEnter(Collision collision)
    {
        IsGrounded = true;
    }
    private void OnCollisionExit(Collision collision)
    {
        IsGrounded = false;
    }
}